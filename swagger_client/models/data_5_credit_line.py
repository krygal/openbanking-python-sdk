# coding: utf-8

"""
    Account and Transaction API Specification

    Swagger for Account and Transaction API Specification

    OpenAPI spec version: v1.1.0
    Contact: ServiceDesk@openbanking.org.uk
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class Data5CreditLine(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'included': 'bool',
        'amount': 'Data5Amount1',
        'type': 'str'
    }

    attribute_map = {
        'included': 'Included',
        'amount': 'Amount',
        'type': 'Type'
    }

    def __init__(self, included=None, amount=None, type=None):
        """
        Data5CreditLine - a model defined in Swagger
        """

        self._included = None
        self._amount = None
        self._type = None

        self.included = included
        if amount is not None:
          self.amount = amount
        if type is not None:
          self.type = type

    @property
    def included(self):
        """
        Gets the included of this Data5CreditLine.
        Indicates whether or not the credit line is included in the balance of the account. Usage: If not present, credit line is not included in the balance amount of the account.

        :return: The included of this Data5CreditLine.
        :rtype: bool
        """
        return self._included

    @included.setter
    def included(self, included):
        """
        Sets the included of this Data5CreditLine.
        Indicates whether or not the credit line is included in the balance of the account. Usage: If not present, credit line is not included in the balance amount of the account.

        :param included: The included of this Data5CreditLine.
        :type: bool
        """
        if included is None:
            raise ValueError("Invalid value for `included`, must not be `None`")

        self._included = included

    @property
    def amount(self):
        """
        Gets the amount of this Data5CreditLine.

        :return: The amount of this Data5CreditLine.
        :rtype: Data5Amount1
        """
        return self._amount

    @amount.setter
    def amount(self, amount):
        """
        Sets the amount of this Data5CreditLine.

        :param amount: The amount of this Data5CreditLine.
        :type: Data5Amount1
        """

        self._amount = amount

    @property
    def type(self):
        """
        Gets the type of this Data5CreditLine.
        Limit type, in a coded form.

        :return: The type of this Data5CreditLine.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """
        Sets the type of this Data5CreditLine.
        Limit type, in a coded form.

        :param type: The type of this Data5CreditLine.
        :type: str
        """
        allowed_values = ["Pre-Agreed", "Emergency", "Temporary"]
        if type not in allowed_values:
            raise ValueError(
                "Invalid value for `type` ({0}), must be one of {1}"
                .format(type, allowed_values)
            )

        self._type = type

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, Data5CreditLine):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
