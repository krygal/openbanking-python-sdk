# coding: utf-8

"""
    Account and Transaction API Specification

    Swagger for Account and Transaction API Specification

    OpenAPI spec version: v1.1.0
    Contact: ServiceDesk@openbanking.org.uk
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class Data(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'permissions': 'list[str]',
        'expiration_date_time': 'datetime',
        'transaction_from_date_time': 'datetime',
        'transaction_to_date_time': 'datetime'
    }

    attribute_map = {
        'permissions': 'Permissions',
        'expiration_date_time': 'ExpirationDateTime',
        'transaction_from_date_time': 'TransactionFromDateTime',
        'transaction_to_date_time': 'TransactionToDateTime'
    }

    def __init__(self, permissions=None, expiration_date_time=None, transaction_from_date_time=None, transaction_to_date_time=None):
        """
        Data - a model defined in Swagger
        """

        self._permissions = None
        self._expiration_date_time = None
        self._transaction_from_date_time = None
        self._transaction_to_date_time = None

        self.permissions = permissions
        if expiration_date_time is not None:
          self.expiration_date_time = expiration_date_time
        if transaction_from_date_time is not None:
          self.transaction_from_date_time = transaction_from_date_time
        if transaction_to_date_time is not None:
          self.transaction_to_date_time = transaction_to_date_time

    @property
    def permissions(self):
        """
        Gets the permissions of this Data.
        Specifies the Open Banking account request types. This is a list of the data clusters being consented by the PSU, and requested for authorisation with the ASPSP.

        :return: The permissions of this Data.
        :rtype: list[str]
        """
        return self._permissions

    @permissions.setter
    def permissions(self, permissions):
        """
        Sets the permissions of this Data.
        Specifies the Open Banking account request types. This is a list of the data clusters being consented by the PSU, and requested for authorisation with the ASPSP.

        :param permissions: The permissions of this Data.
        :type: list[str]
        """
        if permissions is None:
            raise ValueError("Invalid value for `permissions`, must not be `None`")
        allowed_values = ["ReadAccountsBasic", "ReadAccountsDetail", "ReadBalances", "ReadBeneficiariesBasic", "ReadBeneficiariesDetail", "ReadDirectDebits", "ReadProducts", "ReadStandingOrdersBasic", "ReadStandingOrdersDetail", "ReadTransactionsBasic", "ReadTransactionsCredits", "ReadTransactionsDebits", "ReadTransactionsDetail"]
        if not set(permissions).issubset(set(allowed_values)):
            raise ValueError(
                "Invalid values for `permissions` [{0}], must be a subset of [{1}]"
                .format(", ".join(map(str, set(permissions)-set(allowed_values))),
                        ", ".join(map(str, allowed_values)))
            )

        self._permissions = permissions

    @property
    def expiration_date_time(self):
        """
        Gets the expiration_date_time of this Data.
        Specified date and time the permissions will expire. If this is not populated, the permissions will be open ended.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00

        :return: The expiration_date_time of this Data.
        :rtype: datetime
        """
        return self._expiration_date_time

    @expiration_date_time.setter
    def expiration_date_time(self, expiration_date_time):
        """
        Sets the expiration_date_time of this Data.
        Specified date and time the permissions will expire. If this is not populated, the permissions will be open ended.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00

        :param expiration_date_time: The expiration_date_time of this Data.
        :type: datetime
        """

        self._expiration_date_time = expiration_date_time

    @property
    def transaction_from_date_time(self):
        """
        Gets the transaction_from_date_time of this Data.
        Specified start date and time for the transaction query period. If this is not populated, the start date will be open ended, and data will be returned from the earliest available transaction.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00

        :return: The transaction_from_date_time of this Data.
        :rtype: datetime
        """
        return self._transaction_from_date_time

    @transaction_from_date_time.setter
    def transaction_from_date_time(self, transaction_from_date_time):
        """
        Sets the transaction_from_date_time of this Data.
        Specified start date and time for the transaction query period. If this is not populated, the start date will be open ended, and data will be returned from the earliest available transaction.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00

        :param transaction_from_date_time: The transaction_from_date_time of this Data.
        :type: datetime
        """

        self._transaction_from_date_time = transaction_from_date_time

    @property
    def transaction_to_date_time(self):
        """
        Gets the transaction_to_date_time of this Data.
        Specified end date and time for the transaction query period. If this is not populated, the end date will be open ended, and data will be returned to the latest available transaction.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00

        :return: The transaction_to_date_time of this Data.
        :rtype: datetime
        """
        return self._transaction_to_date_time

    @transaction_to_date_time.setter
    def transaction_to_date_time(self, transaction_to_date_time):
        """
        Sets the transaction_to_date_time of this Data.
        Specified end date and time for the transaction query period. If this is not populated, the end date will be open ended, and data will be returned to the latest available transaction.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00

        :param transaction_to_date_time: The transaction_to_date_time of this Data.
        :type: datetime
        """

        self._transaction_to_date_time = transaction_to_date_time

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, Data):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
