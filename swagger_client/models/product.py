# coding: utf-8

"""
    Account and Transaction API Specification

    Swagger for Account and Transaction API Specification

    OpenAPI spec version: v1.1.0
    Contact: ServiceDesk@openbanking.org.uk
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class Product(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'account_id': 'str',
        'product_identifier': 'str',
        'product_type': 'str',
        'product_name': 'str',
        'secondary_product_identifier': 'str'
    }

    attribute_map = {
        'account_id': 'AccountId',
        'product_identifier': 'ProductIdentifier',
        'product_type': 'ProductType',
        'product_name': 'ProductName',
        'secondary_product_identifier': 'SecondaryProductIdentifier'
    }

    def __init__(self, account_id=None, product_identifier=None, product_type=None, product_name=None, secondary_product_identifier=None):
        """
        Product - a model defined in Swagger
        """

        self._account_id = None
        self._product_identifier = None
        self._product_type = None
        self._product_name = None
        self._secondary_product_identifier = None

        self.account_id = account_id
        self.product_identifier = product_identifier
        self.product_type = product_type
        if product_name is not None:
          self.product_name = product_name
        if secondary_product_identifier is not None:
          self.secondary_product_identifier = secondary_product_identifier

    @property
    def account_id(self):
        """
        Gets the account_id of this Product.
        A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner.

        :return: The account_id of this Product.
        :rtype: str
        """
        return self._account_id

    @account_id.setter
    def account_id(self, account_id):
        """
        Sets the account_id of this Product.
        A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner.

        :param account_id: The account_id of this Product.
        :type: str
        """
        if account_id is None:
            raise ValueError("Invalid value for `account_id`, must not be `None`")
        if account_id is not None and len(account_id) > 40:
            raise ValueError("Invalid value for `account_id`, length must be less than or equal to `40`")
        if account_id is not None and len(account_id) < 1:
            raise ValueError("Invalid value for `account_id`, length must be greater than or equal to `1`")

        self._account_id = account_id

    @property
    def product_identifier(self):
        """
        Gets the product_identifier of this Product.
        Identifier within the parent organisation for the product. Must be unique in the organisation.

        :return: The product_identifier of this Product.
        :rtype: str
        """
        return self._product_identifier

    @product_identifier.setter
    def product_identifier(self, product_identifier):
        """
        Sets the product_identifier of this Product.
        Identifier within the parent organisation for the product. Must be unique in the organisation.

        :param product_identifier: The product_identifier of this Product.
        :type: str
        """
        if product_identifier is None:
            raise ValueError("Invalid value for `product_identifier`, must not be `None`")

        self._product_identifier = product_identifier

    @property
    def product_type(self):
        """
        Gets the product_type of this Product.
        Descriptive code for the product category.

        :return: The product_type of this Product.
        :rtype: str
        """
        return self._product_type

    @product_type.setter
    def product_type(self, product_type):
        """
        Sets the product_type of this Product.
        Descriptive code for the product category.

        :param product_type: The product_type of this Product.
        :type: str
        """
        if product_type is None:
            raise ValueError("Invalid value for `product_type`, must not be `None`")
        allowed_values = ["BCA", "PCA"]
        if product_type not in allowed_values:
            raise ValueError(
                "Invalid value for `product_type` ({0}), must be one of {1}"
                .format(product_type, allowed_values)
            )

        self._product_type = product_type

    @property
    def product_name(self):
        """
        Gets the product_name of this Product.
        The name of the product used for marketing purposes from a customer perspective. I.e. what the customer would recognise.

        :return: The product_name of this Product.
        :rtype: str
        """
        return self._product_name

    @product_name.setter
    def product_name(self, product_name):
        """
        Sets the product_name of this Product.
        The name of the product used for marketing purposes from a customer perspective. I.e. what the customer would recognise.

        :param product_name: The product_name of this Product.
        :type: str
        """

        self._product_name = product_name

    @property
    def secondary_product_identifier(self):
        """
        Gets the secondary_product_identifier of this Product.
        Identifier within the parent organisation for the product. Must be unique in the organisation.

        :return: The secondary_product_identifier of this Product.
        :rtype: str
        """
        return self._secondary_product_identifier

    @secondary_product_identifier.setter
    def secondary_product_identifier(self, secondary_product_identifier):
        """
        Sets the secondary_product_identifier of this Product.
        Identifier within the parent organisation for the product. Must be unique in the organisation.

        :param secondary_product_identifier: The secondary_product_identifier of this Product.
        :type: str
        """

        self._secondary_product_identifier = secondary_product_identifier

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, Product):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
