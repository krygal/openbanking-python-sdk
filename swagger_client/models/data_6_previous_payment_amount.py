# coding: utf-8

"""
    Account and Transaction API Specification

    Swagger for Account and Transaction API Specification

    OpenAPI spec version: v1.1.0
    Contact: ServiceDesk@openbanking.org.uk
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from pprint import pformat
from six import iteritems
import re


class Data6PreviousPaymentAmount(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """


    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'amount': 'str',
        'currency': 'str'
    }

    attribute_map = {
        'amount': 'Amount',
        'currency': 'Currency'
    }

    def __init__(self, amount=None, currency=None):
        """
        Data6PreviousPaymentAmount - a model defined in Swagger
        """

        self._amount = None
        self._currency = None

        self.amount = amount
        self.currency = currency

    @property
    def amount(self):
        """
        Gets the amount of this Data6PreviousPaymentAmount.

        :return: The amount of this Data6PreviousPaymentAmount.
        :rtype: str
        """
        return self._amount

    @amount.setter
    def amount(self, amount):
        """
        Sets the amount of this Data6PreviousPaymentAmount.

        :param amount: The amount of this Data6PreviousPaymentAmount.
        :type: str
        """
        if amount is None:
            raise ValueError("Invalid value for `amount`, must not be `None`")
        if amount is not None and not re.search('^\\d{1,13}\\.\\d{1,5}$', amount):
            raise ValueError("Invalid value for `amount`, must be a follow pattern or equal to `/^\\d{1,13}\\.\\d{1,5}$/`")

        self._amount = amount

    @property
    def currency(self):
        """
        Gets the currency of this Data6PreviousPaymentAmount.
        A code allocated to a currency by a Maintenance Agency under an international identification scheme, as described in the latest edition of the international standard ISO 4217 'Codes for the representation of currencies and funds'

        :return: The currency of this Data6PreviousPaymentAmount.
        :rtype: str
        """
        return self._currency

    @currency.setter
    def currency(self, currency):
        """
        Sets the currency of this Data6PreviousPaymentAmount.
        A code allocated to a currency by a Maintenance Agency under an international identification scheme, as described in the latest edition of the international standard ISO 4217 'Codes for the representation of currencies and funds'

        :param currency: The currency of this Data6PreviousPaymentAmount.
        :type: str
        """
        if currency is None:
            raise ValueError("Invalid value for `currency`, must not be `None`")
        if currency is not None and not re.search('^[A-Z]{3,3}$', currency):
            raise ValueError("Invalid value for `currency`, must be a follow pattern or equal to `/^[A-Z]{3,3}$/`")

        self._currency = currency

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        if not isinstance(other, Data6PreviousPaymentAmount):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
