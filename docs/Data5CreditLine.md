# Data5CreditLine

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**included** | **bool** | Indicates whether or not the credit line is included in the balance of the account. Usage: If not present, credit line is not included in the balance amount of the account. | 
**amount** | [**Data5Amount1**](Data5Amount1.md) |  | [optional] 
**type** | **str** | Limit type, in a coded form. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


