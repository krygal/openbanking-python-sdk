# DirectDebit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner. | 
**direct_debit_id** | **str** | A unique and immutable identifier used to identify the direct debit resource. This identifier has no meaning to the account owner. | [optional] 
**mandate_identification** | **str** | Direct Debit reference. For AUDDIS service users provide Core Reference. For non AUDDIS service users provide Core reference if possible or last used reference. | 
**direct_debit_status_code** | **str** | Specifies the status of the direct debit in code form. | [optional] 
**name** | **str** | Name of Service User | 
**previous_payment_date_time** | **datetime** | Date of most recent direct debit collection.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00 | [optional] 
**previous_payment_amount** | [**Data6PreviousPaymentAmount**](Data6PreviousPaymentAmount.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


