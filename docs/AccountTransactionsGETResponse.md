# AccountTransactionsGETResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Data3**](Data3.md) |  | [optional] 
**links** | [**Links**](Links.md) |  | [optional] 
**meta** | [**MetaData**](MetaData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


