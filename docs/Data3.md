# Data3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction** | [**list[Data3Transaction]**](Data3Transaction.md) | Transaction | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


