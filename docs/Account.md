# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner. | 
**currency** | **str** | Identification of the currency in which the account is held.  Usage: Currency should only be used in case one and the same account number covers several currencies and the initiating party needs to identify which currency needs to be used for settlement on the account. | 
**nickname** | **str** | The nickname of the account, assigned by the account owner in order to provide an additional means of identification of the account. | [optional] 
**account** | [**Data2Account**](Data2Account.md) |  | [optional] 
**servicer** | [**Data2Servicer**](Data2Servicer.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


