# Data8

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**list[Product]**](Product.md) | Product | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


