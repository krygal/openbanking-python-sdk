# Data3Balance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | [**Data3BalanceAmount**](Data3BalanceAmount.md) |  | [optional] 
**credit_debit_indicator** | **str** | Indicates whether the balance is a credit or a debit balance. Usage: A zero balance is considered to be a credit balance. | 
**type** | **str** | Balance type, in a coded form. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


