# Data3ProprietaryBankTransactionCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** | Proprietary bank transaction code to identify the underlying transaction. | 
**issuer** | **str** | Identification of the issuer of the proprietary bank transaction code. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


