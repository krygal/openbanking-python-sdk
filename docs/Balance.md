# Balance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner. | 
**amount** | [**Data5Amount**](Data5Amount.md) |  | [optional] 
**credit_debit_indicator** | **str** | Indicates whether the balance is a credit or a debit balance. Usage: A zero balance is considered to be a credit balance. | 
**type** | **str** | Balance type, in a coded form. | 
**date_time** | **datetime** | Indicates the date (and time) of the balance.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00 | 
**credit_line** | [**list[Data5CreditLine]**](Data5CreditLine.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


