# Data3MerchantDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merchant_name** | **str** | Name by which the merchant is known. | [optional] 
**merchant_category_code** | **str** | Category code conform to ISO 18245, related to the type of services or goods the merchant provides for the transaction. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


