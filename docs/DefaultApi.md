# swagger_client.DefaultApi

All URIs are relative to *https://localhost/open-banking/v1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_account_request**](DefaultApi.md#create_account_request) | **POST** /account-requests | Create an account request
[**delete_account_request**](DefaultApi.md#delete_account_request) | **DELETE** /account-requests/{AccountRequestId} | Delete an account request
[**get_account**](DefaultApi.md#get_account) | **GET** /accounts/{AccountId} | Get Account
[**get_account_balances**](DefaultApi.md#get_account_balances) | **GET** /accounts/{AccountId}/balances | Get Account Balances
[**get_account_beneficiaries**](DefaultApi.md#get_account_beneficiaries) | **GET** /accounts/{AccountId}/beneficiaries | Get Account Beneficiaries
[**get_account_direct_debits**](DefaultApi.md#get_account_direct_debits) | **GET** /accounts/{AccountId}/direct-debits | Get Account Direct Debits
[**get_account_product**](DefaultApi.md#get_account_product) | **GET** /accounts/{AccountId}/product | Get Account Product
[**get_account_request**](DefaultApi.md#get_account_request) | **GET** /account-requests/{AccountRequestId} | Get an account request
[**get_account_standing_orders**](DefaultApi.md#get_account_standing_orders) | **GET** /accounts/{AccountId}/standing-orders | Get Account Standing Orders
[**get_account_transactions**](DefaultApi.md#get_account_transactions) | **GET** /accounts/{AccountId}/transactions | Get Account Transactions
[**get_accounts**](DefaultApi.md#get_accounts) | **GET** /accounts | Get Accounts
[**get_balances**](DefaultApi.md#get_balances) | **GET** /balances | Get Balances
[**get_beneficiaries**](DefaultApi.md#get_beneficiaries) | **GET** /beneficiaries | Get Beneficiaries
[**get_direct_debits**](DefaultApi.md#get_direct_debits) | **GET** /direct-debits | Get Direct Debits
[**get_products**](DefaultApi.md#get_products) | **GET** /products | Get Products
[**get_standing_orders**](DefaultApi.md#get_standing_orders) | **GET** /standing-orders | Get Standing Orders
[**get_transactions**](DefaultApi.md#get_transactions) | **GET** /transactions | Get Transactions


# **create_account_request**
> AccountRequestPOSTResponse create_account_request(body, x_fapi_financial_id, authorization, x_jws_signature, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Create an account request

Create an account request

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: TPPOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
body = swagger_client.AccountRequestPOSTRequest() # AccountRequestPOSTRequest | Create an Account Request
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_jws_signature = 'x_jws_signature_example' # str | Header containing a detached JWS signature of the body of the payload.
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Create an account request
    api_response = api_instance.create_account_request(body, x_fapi_financial_id, authorization, x_jws_signature, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_account_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AccountRequestPOSTRequest**](AccountRequestPOSTRequest.md)| Create an Account Request | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_jws_signature** | **str**| Header containing a detached JWS signature of the body of the payload. | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**AccountRequestPOSTResponse**](AccountRequestPOSTResponse.md)

### Authorization

[TPPOAuth2Security](../README.md#TPPOAuth2Security)

### HTTP request headers

 - **Content-Type**: application/json; charset=utf-8
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_account_request**
> delete_account_request(account_request_id, authorization, x_fapi_financial_id)

Delete an account request

Delete an account request

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: TPPOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_request_id = 'account_request_id_example' # str | Unique identification as assigned by the ASPSP to uniquely identify the account request resource.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.

try: 
    # Delete an account request
    api_instance.delete_account_request(account_request_id, authorization, x_fapi_financial_id)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_account_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_request_id** | **str**| Unique identification as assigned by the ASPSP to uniquely identify the account request resource. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 

### Return type

void (empty response body)

### Authorization

[TPPOAuth2Security](../README.md#TPPOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account**
> AccountGETResponse get_account(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Account

Get an account

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_id = 'account_id_example' # str | A unique identifier used to identify the account resource.
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Account
    api_response = api_instance.get_account(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| A unique identifier used to identify the account resource. | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**AccountGETResponse**](AccountGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_balances**
> BalancesGETResponse get_account_balances(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Account Balances

Get Balances related to an account

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_id = 'account_id_example' # str | A unique identifier used to identify the account resource.
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Account Balances
    api_response = api_instance.get_account_balances(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account_balances: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| A unique identifier used to identify the account resource. | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**BalancesGETResponse**](BalancesGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_beneficiaries**
> BeneficiariesGETResponse get_account_beneficiaries(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Account Beneficiaries

Get Beneficiaries related to an account

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_id = 'account_id_example' # str | A unique identifier used to identify the account resource.
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Account Beneficiaries
    api_response = api_instance.get_account_beneficiaries(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account_beneficiaries: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| A unique identifier used to identify the account resource. | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**BeneficiariesGETResponse**](BeneficiariesGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_direct_debits**
> AccountGETResponse1 get_account_direct_debits(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Account Direct Debits

Get Direct Debits related to an account

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_id = 'account_id_example' # str | A unique identifier used to identify the account resource.
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Account Direct Debits
    api_response = api_instance.get_account_direct_debits(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account_direct_debits: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| A unique identifier used to identify the account resource. | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**AccountGETResponse1**](AccountGETResponse1.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_product**
> ProductGETResponse get_account_product(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Account Product

Get Product related to an account

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_id = 'account_id_example' # str | A unique identifier used to identify the account resource.
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Account Product
    api_response = api_instance.get_account_product(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account_product: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| A unique identifier used to identify the account resource. | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**ProductGETResponse**](ProductGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_request**
> AccountRequestPOSTResponse get_account_request(account_request_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get an account request

Get an account request

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: TPPOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_request_id = 'account_request_id_example' # str | Unique identification as assigned by the ASPSP to uniquely identify the account request resource.
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get an account request
    api_response = api_instance.get_account_request(account_request_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_request_id** | **str**| Unique identification as assigned by the ASPSP to uniquely identify the account request resource. | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**AccountRequestPOSTResponse**](AccountRequestPOSTResponse.md)

### Authorization

[TPPOAuth2Security](../README.md#TPPOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_standing_orders**
> StandingOrdersGETResponse get_account_standing_orders(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Account Standing Orders

Get Standing Orders related to an account

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_id = 'account_id_example' # str | A unique identifier used to identify the account resource.
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Account Standing Orders
    api_response = api_instance.get_account_standing_orders(account_id, x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account_standing_orders: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| A unique identifier used to identify the account resource. | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**StandingOrdersGETResponse**](StandingOrdersGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_transactions**
> AccountTransactionsGETResponse get_account_transactions(account_id, x_fapi_financial_id, authorization, from_booking_date_time=from_booking_date_time, to_booking_date_time=to_booking_date_time, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Account Transactions

Get transactions related to an account

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
account_id = 'account_id_example' # str | A unique identifier used to identify the account resource.
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
from_booking_date_time = '2013-10-20T19:20:30+01:00' # datetime | The UTC ISO 8601 Date Time to filter transactions FROM  NB Time component is optional - set to 00:00:00 for just Date.   The parameter must NOT have a timezone set (optional)
to_booking_date_time = '2013-10-20T19:20:30+01:00' # datetime | The UTC ISO 8601 Date Time to filter transactions TO  NB Time component is optional - set to 00:00:00 for just Date.   The parameter must NOT have a timezone set (optional)
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Account Transactions
    api_response = api_instance.get_account_transactions(account_id, x_fapi_financial_id, authorization, from_booking_date_time=from_booking_date_time, to_booking_date_time=to_booking_date_time, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account_transactions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_id** | **str**| A unique identifier used to identify the account resource. | 
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **from_booking_date_time** | **datetime**| The UTC ISO 8601 Date Time to filter transactions FROM  NB Time component is optional - set to 00:00:00 for just Date.   The parameter must NOT have a timezone set | [optional] 
 **to_booking_date_time** | **datetime**| The UTC ISO 8601 Date Time to filter transactions TO  NB Time component is optional - set to 00:00:00 for just Date.   The parameter must NOT have a timezone set | [optional] 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**AccountTransactionsGETResponse**](AccountTransactionsGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_accounts**
> AccountGETResponse get_accounts(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Accounts

Get a list of accounts

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Accounts
    api_response = api_instance.get_accounts(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_accounts: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**AccountGETResponse**](AccountGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_balances**
> BalancesGETResponse get_balances(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Balances

Get Balances

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Balances
    api_response = api_instance.get_balances(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_balances: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**BalancesGETResponse**](BalancesGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_beneficiaries**
> BeneficiariesGETResponse get_beneficiaries(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Beneficiaries

Get Beneficiaries

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Beneficiaries
    api_response = api_instance.get_beneficiaries(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_beneficiaries: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**BeneficiariesGETResponse**](BeneficiariesGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_direct_debits**
> AccountGETResponse1 get_direct_debits(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Direct Debits

Get Direct Debits

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Direct Debits
    api_response = api_instance.get_direct_debits(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_direct_debits: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**AccountGETResponse1**](AccountGETResponse1.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_products**
> ProductGETResponse get_products(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Products

Get Products

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Products
    api_response = api_instance.get_products(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_products: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**ProductGETResponse**](ProductGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_standing_orders**
> StandingOrdersGETResponse get_standing_orders(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)

Get Standing Orders

Get Standing Orders

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)

try: 
    # Get Standing Orders
    api_response = api_instance.get_standing_orders(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_standing_orders: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 

### Return type

[**StandingOrdersGETResponse**](StandingOrdersGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transactions**
> AccountTransactionsGETResponse get_transactions(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id, from_booking_date_time=from_booking_date_time, to_booking_date_time=to_booking_date_time)

Get Transactions

Get Transactions

### Example 
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: PSUOAuth2Security
swagger_client.configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
x_fapi_financial_id = 'x_fapi_financial_id_example' # str | The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.
authorization = 'authorization_example' # str | An Authorisation Token as per https://tools.ietf.org/html/rfc6750
x_fapi_customer_last_logged_time = 'x_fapi_customer_last_logged_time_example' # str | The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC (optional)
x_fapi_customer_ip_address = 'x_fapi_customer_ip_address_example' # str | The PSU's IP address if the PSU is currently logged in with the TPP. (optional)
x_fapi_interaction_id = 'x_fapi_interaction_id_example' # str | An RFC4122 UID used as a correlation id. (optional)
from_booking_date_time = '2013-10-20T19:20:30+01:00' # datetime | The UTC ISO 8601 Date Time to filter transactions FROM  NB Time component is optional - set to 00:00:00 for just Date.   The parameter must NOT have a timezone set (optional)
to_booking_date_time = '2013-10-20T19:20:30+01:00' # datetime | The UTC ISO 8601 Date Time to filter transactions TO  NB Time component is optional - set to 00:00:00 for just Date.   The parameter must NOT have a timezone set (optional)

try: 
    # Get Transactions
    api_response = api_instance.get_transactions(x_fapi_financial_id, authorization, x_fapi_customer_last_logged_time=x_fapi_customer_last_logged_time, x_fapi_customer_ip_address=x_fapi_customer_ip_address, x_fapi_interaction_id=x_fapi_interaction_id, from_booking_date_time=from_booking_date_time, to_booking_date_time=to_booking_date_time)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_transactions: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **x_fapi_financial_id** | **str**| The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB. | 
 **authorization** | **str**| An Authorisation Token as per https://tools.ietf.org/html/rfc6750 | 
 **x_fapi_customer_last_logged_time** | **str**| The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC | [optional] 
 **x_fapi_customer_ip_address** | **str**| The PSU&#39;s IP address if the PSU is currently logged in with the TPP. | [optional] 
 **x_fapi_interaction_id** | **str**| An RFC4122 UID used as a correlation id. | [optional] 
 **from_booking_date_time** | **datetime**| The UTC ISO 8601 Date Time to filter transactions FROM  NB Time component is optional - set to 00:00:00 for just Date.   The parameter must NOT have a timezone set | [optional] 
 **to_booking_date_time** | **datetime**| The UTC ISO 8601 Date Time to filter transactions TO  NB Time component is optional - set to 00:00:00 for just Date.   The parameter must NOT have a timezone set | [optional] 

### Return type

[**AccountTransactionsGETResponse**](AccountTransactionsGETResponse.md)

### Authorization

[PSUOAuth2Security](../README.md#PSUOAuth2Security)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

