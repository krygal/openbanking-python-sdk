# Data3Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner. | 
**transaction_id** | **str** | Unique identifier for the transaction within an servicing institution. This identifier is both unique and immutable. | [optional] 
**transaction_reference** | **str** | Unique reference for the transaction. This reference is optionally populated, and may as an example be the FPID in the Faster Payments context. | [optional] 
**amount** | [**Data3Amount**](Data3Amount.md) |  | [optional] 
**credit_debit_indicator** | **str** | Indicates whether the transaction is a credit or a debit entry. | 
**status** | **str** | Status of a transaction entry on the books of the account servicer. | 
**booking_date_time** | **datetime** | Date and time when a transaction entry is posted to an account on the account servicer&#39;s books. Usage: Booking date is the expected booking date, unless the status is booked, in which case it is the actual booking date.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00 | 
**value_date_time** | **datetime** | Date and time at which assets become available to the account owner in case of a credit entry, or cease to be available to the account owner in case of a debit entry.  Usage: If entry status is pending and value date is present, then the value date refers to an expected/requested value date. For entries subject to availability/float and for which availability information is provided, the value date must not be used. In this case the availability component identifies the  number of availability days.  All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00 | [optional] 
**transaction_information** | **str** | Further details of the transaction. This is the transaction narrative, which is unstructured text. | [optional] 
**address_line** | **str** | Information that locates and identifies a specific address, as defined by postal services, that is presented in free format text. | [optional] 
**bank_transaction_code** | [**Data3BankTransactionCode**](Data3BankTransactionCode.md) |  | [optional] 
**proprietary_bank_transaction_code** | [**Data3ProprietaryBankTransactionCode**](Data3ProprietaryBankTransactionCode.md) |  | [optional] 
**balance** | [**Data3Balance**](Data3Balance.md) |  | [optional] 
**merchant_details** | [**Data3MerchantDetails**](Data3MerchantDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


