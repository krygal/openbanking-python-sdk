# Data5

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**balance** | [**list[Balance]**](Balance.md) | Balance | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


