# Data6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**direct_debit** | [**list[DirectDebit]**](DirectDebit.md) | DirectDebit | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


