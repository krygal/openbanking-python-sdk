# Data4

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beneficiary** | [**list[Beneficiary]**](Beneficiary.md) | Beneficiary | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


