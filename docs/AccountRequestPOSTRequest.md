# AccountRequestPOSTRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**Data**](Data.md) |  | [optional] 
**risk** | **object** | The Risk payload is sent by the initiating party to the ASPSP. It is used to specify additional details for risk scoring for Account Info. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


