# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **str** | A unique and immutable identifier used to identify the account resource. This identifier has no meaning to the account owner. | 
**product_identifier** | **str** | Identifier within the parent organisation for the product. Must be unique in the organisation. | 
**product_type** | **str** | Descriptive code for the product category. | 
**product_name** | **str** | The name of the product used for marketing purposes from a customer perspective. I.e. what the customer would recognise. | [optional] 
**secondary_product_identifier** | **str** | Identifier within the parent organisation for the product. Must be unique in the organisation. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


