# coding: utf-8

"""
    Account and Transaction API Specification

    Swagger for Account and Transaction API Specification

    OpenAPI spec version: v1.1.0
    Contact: ServiceDesk@openbanking.org.uk
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import os
import sys
import unittest

import swagger_client
from swagger_client.rest import ApiException
from swagger_client.models.data_8 import Data8


class TestData8(unittest.TestCase):
    """ Data8 unit test stubs """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testData8(self):
        """
        Test Data8
        """
        # FIXME: construct object with mandatory attributes with example values
        #model = swagger_client.models.data_8.Data8()
        pass


if __name__ == '__main__':
    unittest.main()
